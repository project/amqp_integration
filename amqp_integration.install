<?php

/**
 * @file
 * Requirements check and uninstall hook.
 */

/**
 * Implements hook_uninstall().
 */
function amqp_integration_uninstall() {
  \Drupal::configFactory()->getEditable('amqp_integration.settings')->delete();
}

/**
 * Implements hook_enable().
 */
function amqp_integration_enable() {
  if (!file_exists('../../../../vendor/php-amqplib')) {
    \Drupal::messenger()->addMessage(t('You need to install the PHP AMQP library. See your @status_report for more details!', ['@status_report' => \Drupal::l(t('status report'), \Drupal\Core\Url::fromUri('internal:/admin/reports/status'))]), 'warning');
  }
}

/**
 * Implements hook_requirements().
 */
function amqp_integration_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $requirements['phpamqp_library']['title'] = t('PHP AMQP Library');
    $requirements['phpamqp_library']['description'] = t('The library needed to connect to an AMQP compliant message broker server.');

    if (!file_exists('../../../../vendor/php-amqplib')) {
      $requirements['phpamqp_library']['value'] = t('Installed');
      $requirements['phpamqp_library']['severity'] = REQUIREMENT_OK;
    }
    else {
      $requirements['phpamqp_library']['value'] = t('Not installed, please download from @url and put in a folder with the name "php-amqplib".', ['@url' => \Drupal::l(t('https://github.com/videlalvaro/php-amqplib'), \Drupal\Core\Url::fromUri('https://github.com/videlalvaro/php-amqplib'))]);
      $requirements['phpamqp_library']['severity'] = REQUIREMENT_ERROR;
    }
  }

  return $requirements;
}
