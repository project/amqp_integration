<?php

namespace Drupal\amqp_integration\Commands;

use Drush\Commands\DrushCommands;
use Drupal\message_broker_8\MessageBrokerException;
use Drupal\amqp_integration\AmqpMessageBroker;
use Drupal\amqp_integration\DrushConsumerLogger;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class AmqpCommands extends DrushCommands {

  /**
   * Lists all available AMQP consumers.
   *
   * @command list-amqp-consumers
   * @aliases lc
   * @usage lc
   *   Lists all enabled AMQP message consumers.
   */
  public function listAmqpConsumers() {
    try {
      $broker = message_broker_8_get();
    }
    catch (MessageBrokerException $e) {
      return $this->output()->writeln(dt('Error: The message broker is not properly configured. Wrong credentials or host/port? Please visit admin/config/system/amqp_integration_settings, details: ') . $e->getMessage());
    }

    if (!$broker instanceof AmqpMessageBroker) {
      return $this->output()->writeln(dt('Error: This drush command only works if you set the message broker configuration to AMQP. Please visit admin/config/system/amqp_integration_settings'));
    }

    $consumers = $broker->getConsumers();
    $count_consumers = !empty($consumers) ? count($consumers) : 0;
    $this->output()->writeln(dt("There are @count registered consumers.", ['@count' => $count_consumers]));

    if (!empty($consumers)) {
      foreach ($consumers as $consumer_name => $consumer_info) {
        $this->output()->writeln(dt("@name -> @queue", ['@name' => $consumer_name, '@queue' => $consumer_info['queue']]));
      }
    }

    return TRUE;
  }

  /**
   * Implements the drush command "consume-amqp-messages".
   *
   * @command consume-amqp-messages
   * @aliases cm
   * @option count-messages An option to count the messages
   * @option prefetch_count
   * @usage drush cm
   *   Consume messages from amqp.
   */
  public function consumeAmqpMessages($consumer_name = '', $options = ['count-messages' => FALSE, 'prefetch_count' => FALSE]) {
    $config = \Drupal::config('amqp_integration.settings');
    try {
      $broker = message_broker_8_get();
    }
    catch (MessageBrokerException $e) {
      return $this->output()->writeln(dt('Error: The message broker is not properly configured. Wrong credentials or host/port? Please visit admin/config/system/amqp_integration_settings, details: ') . $e->getMessage());
    }

    if (!$broker instanceof AmqpMessageBroker) {
      return $this->output()->writeln(dt('Error: This drush command only works if you set the message broker configuration to AMQP. Please visit admin/config/system/amqp_integration_settings'));
    }

    $message_broker_host = $config->get('amqp_integration_host');
    $message_broker_port = $config->get('amqp_integration_port');
    $this->output()->writeln(dt('Connected successfully to the AMQP message broker at ') . $message_broker_host . ':' . $message_broker_port);

    if ($options['prefetch_count'] > 0) {
      $broker->ensureQualityOfService($options['prefetch_count']);
      $this->output()->writeln(dt('Prefetch count for current channel was set to @prefetch messages', ['@prefetch' => $options['prefetch_count']]));
    }

    if (empty($consumer_name)) {
      $this->output()->writeln(dt('Starting all consumers ...'));
    }
    else {
      $this->output()->writeln(dt('Starting the consumer "' . $consumer_name . '" ...'));
    }

    $broker->consumeMessages($consumer_name, $options['count-messages'], new DrushConsumerLogger());

    return TRUE;
  }

  /**
   * Implements the drush command "send-amqp-messages".
   *
   * @param int $count
   *   Argument provided to the drush command.
   * @param string $queue
   *   Argument provided to the drush command.
   *
   * @command send-amqp-messages
   * @aliases sm
   * @usage sm
   *   Send messages to amqp server.
   */
  public function sendAmqpMessages($count = 1, $queue = '') {
    try {
      $broker = message_broker_8_get();
    }
    catch (MessageBrokerException $e) {
      return $this->output()->writeln(dt('Error: The message broker is not properly configured. Wrong credentials or host/port? Please visit admin/config/system/amqp_integration_settings, details: ') . $e->getMessage());
    }

    $message = new \StdClass();

    if (!empty($queue)) {
      $message->name = $queue;
    }
    else {
      $message->name = 'queue2';
    }

    $message->body = 'dummy text';
    $broker->sendMessage(json_encode($message), '',
      ['routing_key' => $message->name]);

    $this->output()->writeln(dt('Message successfully sent to @queue', ['@queue' => $message->name]));

    return TRUE;
  }

}
