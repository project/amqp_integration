<?php

namespace Drupal\amqp_integration;

/**
 * Represents a logger that outputs info and error strings.
 */
interface ConsumerLoggerInterface {

  /**
   * Logs an info message.
   *
   * @param string $message
   *   Message to log.
   */
  public function logInfo($message);

  /**
   * Logs an error message.
   *
   * @param string $message
   *   Message to log.
   */
  public function logError($message);

}
