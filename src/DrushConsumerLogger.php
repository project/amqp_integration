<?php

namespace Drupal\amqp_integration;

use Drush\Commands\DrushCommands;

/**
 * DrushConsumerLogger that prints messages to the console.
 */
class DrushConsumerLogger extends DrushCommands implements ConsumerLoggerInterface {

  /**
   * Logs an info message.
   *
   * @param string $message
   *   The message to log.
   */
  public function logInfo($message) {
    $this->output()->writeln($message);
  }

  /**
   * Logs an error message.
   *
   * @param string $message
   *   The message to log.
   */
  public function logError($message) {
    return $this->output()->writeln('Error (' . date('d.m.Y H:i') . '): ' . $message);
  }

}
