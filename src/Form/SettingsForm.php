<?php

namespace Drupal\amqp_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure message broker for using amqp protocol.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amqp_integration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'amqp_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('amqp_integration.settings');
    $form['amqp_integration_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('AMQP server settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['amqp_integration_settings']['intro'] = [
      '#markup' => '<p>' . t('Enter host and credentials for the AMQP compliant message broker you want to use.') . '</p>',
    ];

    $form['amqp_integration_settings']['amqp_integration_host'] = [
      '#type' => 'textfield',
      '#title' => t('Host'),
      '#default_value' => $config->get('amqp_integration_host'),
    ];

    $form['amqp_integration_settings']['amqp_integration_port'] = [
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#default_value' => $config->get('amqp_integration_port'),
    ];

    $form['amqp_integration_settings']['amqp_integration_vhost'] = [
      '#type' => 'textfield',
      '#title' => t('Vhost'),
      '#default_value' => $config->get('amqp_integration_vhost'),
    ];

    $form['amqp_integration_settings']['amqp_integration_username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $config->get('amqp_integration_username'),
    ];

    $form['amqp_integration_settings']['amqp_integration_password'] = [
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $config->get('amqp_integration_password'),
    ];

    $form['amqp_integration_use_ssl'] = [
      '#type' => 'checkbox',
      '#title' => t('Use SSL for connection'),
      '#default_value' => $config->get('amqp_integration_use_ssl'),
    ];

    $form['amqp_integration_ssl'] = [
      '#type' => 'fieldset',
      '#title' => t('SSL settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="amqp_integration_use_ssl"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['amqp_integration_ssl']['amqp_integration_cafile'] = [
      '#type' => 'textfield',
      '#title' => t('CA file'),
      '#states' => [
        'required' => [
          ':input[name="amqp_integration_use_ssl"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => t("Please enter location of Certificate Authority file on local filesystem. For example (/etc/ssl/certs/cacert.org.pem)"),
      '#default_value' => $config->get('amqp_integration_cafile'),
    ];

    $form['amqp_integration_ssl']['amqp_integration_local_cert'] = [
      '#type' => 'textfield',
      '#title' => t('Local cert file'),
      '#states' => [
        'required' => [
          ':input[name="amqp_integration_use_ssl"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => t("Path to local certificate file on filesystem. It must be a PEM encoded file which contains your certificate and private key."),
      '#default_value' => $config->get('amqp_integration_local_cert'),
    ];

    $form['amqp_integration_ssl']['amqp_integration_verify_peer'] = [
      '#type' => 'checkbox',
      '#title' => t('Verify peer'),
      '#default_value' => $config->get('amqp_integration_verify_peer'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->get('amqp_integration_use_ssl') == "1") {
      if (empty($form_state->get('amqp_integration_cafile'))) {
        $form_state->setErrorByName('amqp_integration_cafile', t('Location of Certificate Authority file is required.'));
      }
      elseif (!file_exists($form_state->get('amqp_integration_cafile'))) {
        $form_state->setErrorByName('amqp_integration_cafile', t('The Certificate Authority file was not found, please check the path.'));
      }

      if (empty($form_state->get('amqp_integration_local_cert'))) {
        $form_state->setErrorByName('amqp_integration_local_cert', t('Location of local certificate file is required.'));
      }
      elseif (!file_exists($form_state->get('amqp_integration_local_cert'))) {
        $form_state->setErrorByName('amqp_integration_local_cert', t('The local certificate file was not found, please check the path.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('amqp_integration.settings')
      ->set('amqp_integration_host', $form_state->getValue('amqp_integration_host'))
      ->set('amqp_integration_port', $form_state->getValue('amqp_integration_port'))
      ->set('amqp_integration_vhost', $form_state->getValue('amqp_integration_vhost'))
      ->set('amqp_integration_username', $form_state->getValue('amqp_integration_username'))
      ->set('amqp_integration_password', $form_state->getValue('amqp_integration_password'))
      ->set('amqp_integration_ssl', $form_state->getValue('amqp_integration_ssl'))
      ->set('amqp_integration_cafile', $form_state->getValue('amqp_integration_cafile'))
      ->set('amqp_integration_local_cert', $form_state->getValue('amqp_integration_local_cert'))
      ->set('amqp_integration_verify_peer', $form_state->getValue('amqp_integration_verify_peer'))
      ->save();
  }

}
