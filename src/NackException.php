<?php

namespace Drupal\amqp_integration;

/**
 * An exception a consumer.
 *
 * Should throw to indicate that a message is acknowledged
 * negatively (was not processed successfully).
 */
class NackException extends \Exception {
}
