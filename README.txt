This modules provides AMQP-compatible message broker integration with Drupal 8.
The module requires the php-amqplib library and Message broker 8 module to run.

- Easy settings form to input configuration.
- Error handling
- Custom drush commands for sending, receiving messages and listing queues.

Requirements
===============
- PHP 5.6
- Message broker 8 module.
- php-amqplib library

Installation
===============
Install the module as usual, more info can be found on:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
